var express     = require('express');
var bodyParser  = require('body-parser');

var app         = express(); // Please do not remove this line, since CLI uses this line as guidance to import new controllers


const statusController = require('./controllers/statusController');
app.use('/api/status', statusController);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const server =app.listen(process.env.PORT || 3333, () => {
  console.log('Server is running on prot 3333');
});
module.exports = server;