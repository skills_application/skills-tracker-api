// status controller routes
const router = require('express').Router();

// get /api/status/
router.get('/',(req,res) => {
  res.send('Server is running on port 3333');
});

module.exports = router;